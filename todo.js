let input = document.querySelector('#input-todo');
let addBtn = document.querySelector('.add');

input.addEventListener('keyup',(event)=>{
    
    event.preventDefault();

  if(event.keyCode === 13){
    let localItems = JSON.parse(localStorage.getItem('localItem'));
    if(localItems === null) {
        taskList = [];
    } else {
        taskList = localItems;
    }
    if(input.value !== '') {
        taskList.push(input.value);
    localStorage.setItem('localItem',JSON.stringify(taskList));
    }
   input.value = '';
    showList();
  }
})

addBtn.addEventListener('click',()=>{

    let localItems = JSON.parse(localStorage.getItem('localItem'));
    if(localItems === null) {
        taskList = [];
    } else {
        taskList = localItems;
    }
    if(input.value !== '') {
        taskList.push(input.value);
    localStorage.setItem('localItem',JSON.stringify(taskList));
    }
    
    input.value = '';
    showList();
    
})


const showList = ()=>{

     let ul = document.querySelector('.list-item');
     let output = '';

     let localItems = JSON.parse(localStorage.getItem('localItem'));
     if(localItems === null) {
         taskList = [];
     } else {
         taskList = localItems;
     }

    taskList.forEach((data,index)=> {
        
        output += `<li><input type="checkbox" id="check" unchecked><span>${data}</span>
        <button onclick="editItem(${index})" class="edit">Edit</button>
        <button onclick="deleteItem(${index})" class="remove">Remove</button>
        </li>`;
    })

ul.innerHTML = output;
}
showList();



const deleteItem = (index)=> {
    let localItems = JSON.parse(localStorage.getItem('localItem'));
    taskList.splice(index,1);
    localStorage.setItem('localItem',JSON.stringify(taskList));
    
    showList();
}

const editItem = (index)=> {
    let editInput = document.querySelector('#edit-input');
    let save = document.querySelector('#save');
    let editDiv = document.querySelector('.edit-div');
    let ul = document.querySelector('.list-item');
    ul.style.opacity = 0.5;
    editDiv.style.display = 'block';

    let localItems = JSON.parse(localStorage.getItem('localItem'));
    if(localItems === null) {
        taskList = [];
    } else {
        taskList = localItems;
    }
    let text = taskList[index];
    editInput.value = text;

    save.addEventListener('click',()=> {
        if(editInput.value !== '') {
            taskList[index] = editInput.value;
            localStorage.setItem('localItem',JSON.stringify(taskList))
            editDiv.style.display = 'none';
            ul.style.opacity = 1;
        }
        showList();
    })

    

}

